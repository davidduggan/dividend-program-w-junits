import junit.framework.TestCase;

public class BusinessTest extends TestCase 
{
	//EP
	
	//Test #: 1 
	//Test Objective: Positive Numbers
	//Test Input(s): 2000.0, 500.0, 1500.0
	//Test Expected Output(s): 150.0
	
	public void testdividend001()
	{
		assertEquals(150.0, Business.dividend(2000.0, 500.0, 1500.0));
	}
	
	//Test #: 2 
	//Test Objective: Negative Numbers
	//Test Input(s): -3500.0, -900.0, -2000.0
	//Test Expected Output(s): 0.0
	
	public void testdividend002()
	{
		assertEquals(0.0, Business.dividend(-3500.0, -900.0, -2000.0));
	}
	
	//Test #: 3
	//Test Objective: Zero
	//Test Input(s): 0.0, 0.0, 0.0
	//Test Expected Output(s): 0.0
		
	
	public void testdividend003()
	{
		assertEquals(0.0, Business.dividend(0.0, 0.0, 0.0));
	}
	
	//BVA
		
	//Test #: 4
	//Test Objective: Corrects to 2 decimal places
	//Test Input(s): 2500.3, 450.45, 4000.0 
	//Test Expected Output(s): 102.49
	
	public void testdividend004()
	{
		assertEquals(102.49, Business.dividend(2500.3, 450.45, 4000.0), 0.01);
	}
}
