
public class Business 
{
	public static double dividend(double revenue, double costs, double target)
	{
		double profit = revenue - costs;
		
		if(profit >= target)
		{
			return profit * .1;
		}
		else if(profit > 0)
		{
			return profit * .05;
		}
		else 
		{
			return 0;
		}
	}

	public static void main(String[] args) 
	{
		System.out.println(dividend(2000.0, 500.0, 1500.0));
	}

}
